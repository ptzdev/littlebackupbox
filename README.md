![Little Backup Box](/docs/imgs/LogoLBB.png)

A set of scripts/apps that transform a Raspberry Pi (or any single-board computer running a Debian-based Linux distribution) into an inexpensive, fully-automatic, pocketable photo backup and streaming device.

This "Gadget" is composed by 3 components:

* LittleBackupBox-Core 
* LittleBackupBox-WepApi
* LittleBackupBox-App

![01](/docs/imgs/LandingPage.png)
![02](/docs/imgs/EraseDrive.png)
![03](/docs/imgs/RebootDevice.png)
![04](/docs/imgs/TurnOffDevice.png)